# README #

### What is this repository for? ###

* Getting a tree structure view on JSON

### How do I get set up and run? ###

* Import the repo
* Stringify your JSON (if you have JSON Array, put it inside a JSON)
* Copy the Stringified JSON to ./input/file.txt
* In cmd cd path/to/this/repo
* python -m SimpleHTTPServer 1234
* In web browser 127.0.0.1:1234